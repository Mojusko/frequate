	#=================================================================================
#============================FREQUENCY ANALYSIS TOOLKING==========================
#=================================================================================
#Created by: Mojmir Mutny, The University of Edinburgh (UK), Jacobs University(DE)
#Date: 27 AUG 2013
#Description: Toolking that helps with basic frequency analysis

import sys
import numpy as np
import math
import os

#*********************************************************************************
#*****************************NUMERICAL FREQUENCY ANALYSIS************************
#*********************************************************************************

def Analyze_Numerically(i,o,collum,size,start_interval,end_interval,start_line,end_line,connect): #input,output,collum(starting with 1),size_of_box
	
	boxes = np.zeros(int(float(end_interval-start_interval)/(float(size))),dtype=float)
	lists = np.zeros(int(float(end_interval-start_interval)/(float(size))),dtype=list)
	for y in np.arange(0,len(lists),1):
		lists[y]=[]

	counter=0
	total=0
	sumtot=0
	sumtot_square=0
	# Identifying 
	for line in i:
		counter+=1
		if ((counter>start_line) and ((counter<end_line) or (end_line==-1))):
			line2=line.split()
			line2=map(float, line2)
			done=0
			for ki,k in enumerate(line2):
				if np.isinf(k) or np.isnan(k):
					#print "NaN, Inf Detected"
					done=1
			if done==0:
				index=math.trunc((float(line2[collum-1])-float(start_interval))/size)
				if index<(len(boxes)):
					total=total+1
					boxes[index]+=1
					sumtot+=float(line2[collum-1])
					sumtot_square+=float(line2[collum-1])*float(line2[collum-1])
					if connect>0:
						lists[index].append(line2[int(connect-1)])
		else:
			"WARRNING: Breach of Interval Limit"
			
	print "Total Number of Entries:" , float(total)
	print "Average: ", sumtot/float(total)
	print "Variance: ", sumtot_square/float(total)-(sumtot/float(total))*(sumtot/float(total))
	

	# Printing out the results
	for a in np.arange(0,len(boxes),1):
		if ("-norm" in arguments):
			o.write(str(a+1) + "\t" +str(size*a+float(start_interval)) +"\t" + str(boxes[a]/float(total*size)) + "\t" +"\n" )	
		else:
			o.write(str(a+1) + "\t" +str(size*a+float(start_interval)) +"\t" + str(boxes[a])+ "\t" +"\n" )	


	# Printing the connect group
	if connect>0:
		for a in np.arange(0,len(lists),1):
			o.write("GROUP: " + str(a+1) + "\t" + str(size*a) + "\t" + str(boxes[a]) +"\n")
			for b in np.arange(0,len(lists[a]),1):
				o.write(lists[a][b] + "\t")
				if ((b%3==0) and (b>0) and (b+1<len(lists[a]))):
					o.write("\n")
			o.write("\n")



	return 1


#*********************************************************************************
#*****************************STRING FREQUENCY ANALYSIS***************************
#*********************************************************************************

arguments=[]

for arg in sys.argv:
	arguments.append(arg)

#------------------------------------I/O Handling--------------------------
if ("-h" in arguments):
	print "#############FREQUENCY ANALYSIS TOOLKING ############"
	print "-f Input_file -o Output File"
	print "-start begin_of_interval -end end_of_interval"
	print "-line_start -line_end"
	print "-size size_of_bin"
	print "-norm Normalizes the data"
	sys.exit(0)
else:
	if ("-f" in arguments):
		index=arguments.index("-f")
		name_of_input=arguments[index+1]
		i=open(os.getcwd() + "/" + name_of_input, "rb")
		if (arguments[index+2]):
			collum=int(arguments[index+2])
			print "OK INPUT"
		else:		
			print "ERROR: Missing Collum Number"
			sys.exit(0)
	else:
		print "ERROR: No Input File Specified"
		sys.exit(0)

if ("-o" in arguments):
	index=arguments.index("-o")
	name_of_output=arguments[index+1]
	o = open(os.getcwd() + "/" + name_of_output, "wb")
else:
	print "WARRNING: Output not specified using default file name"
	name_of_output="Output.txt"
	o = open(os.getcwd() + "/" + name_of_output, "wb")

#------------------------------------Parameters--------------------------
if ("-line_start" in arguments):
	index=arguments.index("-line_start")
	line_start=float(arguments[index+1])
else:
	line_start=0

if ("-line_end" in arguments):
	index=arguments.index("-line_end")
	line_end=float(arguments[index+1])
else:
	line_end=-1

if ("-start" in arguments):
	index=arguments.index("-start")
	start_interval=float(arguments[index+1])
else:
	start_interval=0

if ("-end" in arguments):
	index=arguments.index("-end")
	end_interval=float(arguments[index+1])
else:
	end_interval=1000

if ("-size" in arguments):
	index=arguments.index("-size")
	size=float(arguments[index+1])
else:
	size=1

if ("-connect" in arguments):
	index=arguments.index("-connect")
	connect=float(arguments[index+1])
else:
	connect=0
#-------------------------------------------------------------------------

Analyze_Numerically(i,o,collum,size,start_interval,end_interval,line_start,line_end,connect)
